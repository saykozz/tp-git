import csv

nb_mail = 0

with open('data.csv') as file:
    csv_reader_object = csv.reader(file)
    if csv.Sniffer().has_header:
        next(csv_reader_object)
    for row in csv_reader_object:
        nb_mail += 1

print("Number of mail:", nb_mail)